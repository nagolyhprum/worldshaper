/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package readimg;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        // load your image.
        // this creates a color and grayvalue version.
        IMGConverter img = new IMGConverter("Koala.jpg");
        // in case you want to invert the colors/grayvalues, you can do so.
        // looks fancy.
        img.invert();
        // show the image
        img.show();

        // to get the grayvalues, call getGrayValues().
        // this returns a Column x Rows (!!!observe the order!!!) array
        // of values 0..255
        int [][]gv = img.getGrayValues();

        // do whatever you want to with these values.
    }
}
