package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.texture.Texture;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import readimg.IMGConverter;

public class Main extends SimpleApplication implements AnalogListener, PhysicsCollisionListener {

    public static final String NAME = "face257x257.jpg";
    public static final int SIZE = 513;
    public static final float RANGE = 2f;
    public static final float PERSISTENCE = 0.7f;
    public static final float HAIL_FREQ = 0.1f;
    private Spatial spacecraft;
    private TerrainGenerator tg;
    private float xRot, yRot, zRot;
    private static final float TURN_SPEED = FastMath.PI / 4, SPEED = 50;
    private boolean isTurningLeftRight, isTurningUpDown;
    private BulletAppState bas;
    private boolean isFlying;
    private long lastTime;
    private float nextHail;
    private Random hailRand;
    private Material hailMat;
    private Sphere sphere;
    private List<Geometry> hailList;

    public static void main(String[] args) {
        Main app = new Main();
        AppSettings as = new AppSettings(true);
        as.setResolution(1024, 768);
        as.setFrameRate(60);
        as.setVSync(true);
        app.setSettings(as);
        app.setShowSettings(false);
        app.start();
    }
    private boolean resetRequested;

    @Override
    public void simpleInitApp() {
        AudioNode background = new AudioNode(assetManager, "Sounds/background.ogg");
        background.setLooping(true);
        rootNode.attachChild(background);
        background.play();

        AudioNode hover = new AudioNode(assetManager, "Sounds/float.ogg");
        background.setLooping(true);
        hover.play();

        flyCam.setEnabled(false);
        flyCam.setMoveSpeed(100);

        bas = new BulletAppState();
        stateManager.attach(bas);
        bas.getPhysicsSpace().addCollisionListener(this);

        tg = new TerrainGenerator("assets/Textures/" + NAME, SIZE, RANGE, PERSISTENCE);
        tg.smooth(1);
        TerrainQuad tq = new TerrainQuad("face", 100, tg.getSize(), tg.getHeightMap());
        tq.setLocalTranslation(SIZE / 2, 0, SIZE / 2);

        Material face = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        Texture texture = assetManager.loadTexture("Textures/" + NAME);
        //face.setTexture("DiffuseMap", texture);
        face.setColor("Diffuse", ColorRGBA.White);
        face.setColor("Ambient", ColorRGBA.White);
        tq.setMaterial(face);

        hailMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        hailMat.setBoolean("UseMaterialColors", true);
        hailMat.setColor("Ambient", ColorRGBA.DarkGray);
        hailMat.setColor("Diffuse", ColorRGBA.White);
        hailMat.setColor("Specular", ColorRGBA.White);
        hailMat.setFloat("Shininess", 16f); // shininess from 1-128
        sphere = new Sphere(8, 8, 1.5f);
        hailList = new LinkedList<Geometry>();

        RigidBodyControl rbc = new RigidBodyControl(0);
        tq.addControl(rbc);
        bas.getPhysicsSpace().add(rbc);

        rootNode.attachChild(tq);

        spacecraft = assetManager.loadModel("Models/HoverTank/Tank2.mesh.xml");

        CollisionShape cs = CollisionShapeFactory.createBoxShape(spacecraft);
        rbc = new RigidBodyControl(cs, 10000);
        spacecraft.addControl(rbc);
        bas.getPhysicsSpace().add(rbc);
        rbc.setKinematic(true);

        rootNode.attachChild(spacecraft);
        resetGame();

        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);

        inputManager.addMapping("left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("reset", new KeyTrigger(KeyInput.KEY_RETURN));
        inputManager.addListener(this, "left", "right", "up", "down", "reset");

        lastTime = System.currentTimeMillis();

        hailRand = new Random(lastTime);
        nextHail = hailRand.nextFloat() * HAIL_FREQ;
    }

    public void resetGame() {
        RigidBodyControl rbc = (RigidBodyControl) spacecraft.getControl(0);
        rbc.setEnabled(false);
        rbc.setKinematic(isFlying = true);
        spacecraft.setLocalRotation(Matrix3f.IDENTITY);
        spacecraft.rotate(0, yRot = FastMath.rand.nextFloat() * 2 * FastMath.PI, 0);
        spacecraft.setLocalTranslation(SIZE / 2, 0, SIZE / 2);
        BoundingBox bb3d = ((BoundingBox) spacecraft.getWorldBound());
        Vector3f min = new Vector3f(), max = new Vector3f();
        bb3d.getMin(min);
        bb3d.getMax(max);
        Rectangle2D bb2d = new Rectangle2D.Float(min.x, min.z, max.x - min.x, max.z - min.z);
        bb2d = bb2d.createIntersection(new Rectangle(0, 0, SIZE, SIZE));
        float height = Float.MIN_VALUE;
        for (int i = (int) bb2d.getMinX(); i < bb2d.getMaxX(); i++) {
            for (int j = (int) bb2d.getMinY(); j < bb2d.getMaxY(); j++) {
                if (tg.getTrueHeightAtPoint(i, j) > height) {
                    height = tg.getTrueHeightAtPoint(i, j);
                }
            }
        }
        //height = tg.getInterpolatedHeight(SIZE / 2, SIZE / 2);
        spacecraft.setLocalTranslation(SIZE / 2, height + 100, SIZE / 2);
        rbc.setEnabled(true);
        resetRequested = false;
    }

    @Override
    public void simpleUpdate(float tpf) {
        //System.out.println(timeTaken("simpleUpdate start"));
        if (isFlying) {
            spacecraft.move(tpf * FastMath.sin(yRot) * SPEED, -tpf * FastMath.sin(xRot) * SPEED, tpf * FastMath.cos(yRot) * SPEED);
            //System.out.println(timeTaken("ship moved"));
            Vector3f l = spacecraft.getLocalTranslation().clone();
            l.x += Math.sin(yRot + FastMath.PI) * 25 * FastMath.cos(xRot);
            l.y += FastMath.sin(xRot) * 25 + 10;
            l.z += Math.cos(yRot + FastMath.PI) * 25 * FastMath.cos(xRot);
            cam.setLocation(l);

            cam.lookAt(spacecraft.getLocalTranslation(), Vector3f.UNIT_Y);
            //System.out.println(timeTaken("cam moved"));

            if (!isTurningLeftRight) {
                float sign = zRot / FastMath.abs(zRot);
                zRot -= TURN_SPEED * sign * tpf;
                if (zRot / FastMath.abs(zRot) != sign) {
                    zRot = 0;
                }
            }

            if (!isTurningUpDown) {
                float sign = xRot / FastMath.abs(xRot);
                xRot -= TURN_SPEED * sign * tpf;
                if (xRot / FastMath.abs(xRot) != sign) {
                    xRot = 0;
                }
            }
            yRot -= zRot * tpf;
            isTurningLeftRight = false;
            isTurningUpDown = false;
            //System.out.println(timeTaken("ship turn calculated"));

            spacecraft.setLocalRotation(Matrix3f.IDENTITY);
            spacecraft.rotate(xRot, yRot, zRot);
            //System.out.println(timeTaken("ship turned"));
        } else {
        }
        nextHail -= tpf;
        if (nextHail <= 0) {
            nextHail = hailRand.nextFloat() * HAIL_FREQ;
            //System.out.println("Making hail, next at " + nextHail);
            makeHail();
        }
        if (resetRequested) {
            resetGame();
        }
    }

    public void onAnalog(String name, float value, float tpf) {
        float s = TURN_SPEED * tpf;
        if ("left".equals(name)) {
            zRot -= s;
            isTurningLeftRight = true;
        } else if ("right".equals(name)) {
            zRot += s;
            isTurningLeftRight = true;
        }
        if ("up".equals(name)) {
            xRot -= s;
            isTurningUpDown = true;
        } else if ("down".equals(name)) {
            xRot += s;
            isTurningUpDown = true;
        }
        if ("reset".equals(name)) {
            resetRequested = true;
        }
        xRot = FastMath.clamp(xRot, -TURN_SPEED, TURN_SPEED);
        zRot = FastMath.clamp(zRot, -TURN_SPEED, TURN_SPEED);
    }

    public void collision(PhysicsCollisionEvent event) {
        if (!resetRequested) {
            //System.out.println(timeTaken("collision"));
            Spatial s = event.getNodeB();
            if (event.getNodeA().equals(spacecraft)) {
                s = event.getNodeA();
            }
            if (s.equals(spacecraft)) {
                RigidBodyControl rbc = (RigidBodyControl) s.getControl(0);
                if (isFlying) {
                    isFlying = false;
                    rbc.setKinematic(false);
                    flyCam.setEnabled(true);
                    flyCam.setMoveSpeed(100);
                    cam.setLocation(spacecraft.getLocalTranslation().add(0, 25, 0));
                    cam.lookAt(spacecraft.getLocalTranslation(), Vector3f.UNIT_Y);
                }
                float length = rbc.getLinearVelocity().length();
                if (length > 10) {
                    AudioNode bang = new AudioNode(assetManager, "Sounds/bang" + ((int) Math.floor(Math.random() * 3) + 1) + ".ogg");
                    rootNode.attachChild(bang);
                    bang.play();
                }
            }
            //System.out.println(timeTaken("collision done"));
        }
    }

    private String timeTaken(String label) {
        long curTime = System.currentTimeMillis();
        String output = curTime + " " + label + " " + (curTime - lastTime);
        lastTime = curTime;
        return output;
    }

    private void makeHail() {
        Geometry hail = new Geometry("hail", sphere);
        hail.setMaterial(hailMat);
        RigidBodyControl rbc = new RigidBodyControl(1f);
        hail.setLocalTranslation(hailRand.nextFloat() * SIZE, 256, hailRand.nextFloat() * SIZE);
        hail.addControl(rbc);
        bas.getPhysicsSpace().add(rbc);
        rootNode.attachChild(hail);
        hailList.add(hail);
        //remove oldest hailstone if there are too many
        if (hailList.size() > 350) {
            Geometry g = hailList.remove(0);
            g.removeFromParent();
            bas.getPhysicsSpace().remove(g.getControl(RigidBodyControl.class));
        }
    }

    private class TerrainGenerator extends AbstractHeightMap {

        private int[][] pixelData;
        private final float range;
        private final float persistence;

        public TerrainGenerator(String file, int size, float range, float persistence) {
            this.size = size;
            this.range = range;
            this.persistence = persistence;
            try {
                IMGConverter ic = new IMGConverter(file);
                //ic.show();
                pixelData = ic.getGrayValues();
                if (pixelData.length != pixelData[0].length) {
                    throw new Exception("image is not square");
                }
                if (pixelData.length >= size) {
                    throw new Exception("image too large for selected size");
                }
                int checkSize = pixelData.length - 1;
                if ((checkSize & (checkSize - 1)) != 0) {
                    throw new Exception("Size is not 1 + a power of 2");
                }
                load();
            } catch (Exception e) {
                throw new Error(e);
            }
        }

        public int getPixel(int x, int y) {
            return pixelData[(int) ((float) x * pixelData.length / size)][(int) ((float) y * pixelData[0].length / size)];
        }

        @Override
        public boolean load() {
            // clean up data if needed.
            if (null != heightData) {
                unloadHeightMap();
            }
            heightData = new float[size * size];
            float[][] tempBuffer = new float[size][size];
            Random random = new Random(System.currentTimeMillis());

            float offsetRange = range;
            int stepSize = size - 1;
            stepSize /= pixelData.length - 1;

            //seed map with image data
            for (int i = 0; i < pixelData.length; i++) {
                for (int j = 0; j < pixelData[i].length; j++) {
                    tempBuffer[i * stepSize][j * stepSize] = pixelData[i][j];
                }
            }

            //interpolate all other values using diamond-square
            while (stepSize > 1) {
                int[] nextCoords = {0, 0};
                while (nextCoords != null) {
                    nextCoords = doSquareStep(tempBuffer, nextCoords, stepSize, offsetRange, random);
                }
                nextCoords = new int[]{0, 0};
                while (nextCoords != null) {
                    nextCoords = doDiamondStep(tempBuffer, nextCoords, stepSize, offsetRange, random);
                }
                stepSize /= 2;
                offsetRange *= persistence;
            }

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    setHeightAtPoint((float) tempBuffer[i][j], j, i);
                }
            }

            normalizeTerrain(NORMALIZE_RANGE);

            return true;
        }

        protected int[] doSquareStep(float[][] tempBuffer, int[] coords, int stepSize, float offsetRange, Random random) {
            float cornerAverage = 0;
            int x = coords[0];
            int y = coords[1];
            cornerAverage += tempBuffer[x][y];
            cornerAverage += tempBuffer[x + stepSize][y];
            cornerAverage += tempBuffer[x + stepSize][y + stepSize];
            cornerAverage += tempBuffer[x][y + stepSize];
            cornerAverage /= 4;
            float offset = getOffset(random, offsetRange, coords, cornerAverage);
            tempBuffer[x + stepSize / 2][y + stepSize / 2] = cornerAverage + offset;
            //tempBuffer[x + stepSize / 2][y + stepSize / 2] = getPixel(y + stepSize / 2, x + stepSize / 2);

            // Only get to next square if the center is still in map
            if (x + stepSize * 3 / 2 < size) {
                return new int[]{x + stepSize, y};
            }
            if (y + stepSize * 3 / 2 < size) {
                return new int[]{0, y + stepSize};
            }
            return null;
        }

        protected int[] doDiamondStep(float[][] tempBuffer, int[] coords, int stepSize, float offsetRange, Random random) {
            int cornerNbr = 0;
            float cornerAverage = 0;
            int x = coords[0];
            int y = coords[1];
            int[] dxs = new int[]{0, stepSize / 2, stepSize, stepSize / 2};
            int[] dys = new int[]{0, -stepSize / 2, 0, stepSize / 2};

            for (int d = 0; d < 4; d++) {
                int i = x + dxs[d];
                if (i < 0 || i > size - 1) {
                    continue;
                }
                int j = y + dys[d];
                if (j < 0 || j > size - 1) {
                    continue;
                }
                cornerAverage += tempBuffer[i][j];
                cornerNbr++;
            }
            cornerAverage /= cornerNbr;
            float offset = getOffset(random, offsetRange, coords, cornerAverage);
            tempBuffer[x + stepSize / 2][y] = cornerAverage + offset;
            //tempBuffer[x + stepSize / 2][y] = getPixel(y, x + stepSize / 2);

            if (x + stepSize * 3 / 2 < size) {
                return new int[]{x + stepSize, y};
            }
            if (y + stepSize / 2 < size) {
                if (x + stepSize == size - 1) {
                    return new int[]{-stepSize / 2, y + stepSize / 2};
                } else {
                    return new int[]{0, y + stepSize / 2};
                }
            }
            return null;
        }

        /**
         * Generate a random value to add to the computed average
         *
         * @param random the random generator
         * @param offsetRange
         * @param coords
         * @param average
         * @return A semi-random value within offsetRange
         */
        protected float getOffset(Random random, float offsetRange, int[] coords, float average) {
            return 2 * (random.nextFloat() - 0.5F) * offsetRange;
        }
    }
}
